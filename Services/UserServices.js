'use strict';

var Models = require('../Models');

//Get Users from DB
var getUser = function (criteria, projection, options, callback) {
    Models.Users.find(criteria, projection, options, callback);
};
//Insert User in DB
var createUser = function (objToSave, callback) {
    new Models.Users(objToSave).save(callback)
};
//Update User in DB
var updateUser = function (criteria, dataToSet, options, callback) {
    Models.Users.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var updateMulti = function (criteria, dataToSet, options, callback) {
    Models.Users.update(criteria, dataToSet, options, callback);
};

var populateUser = function (criteria, project, options,populateModelArr, callback) {
    Models.Users.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};
module.exports = {
    getUser:getUser,
    createUser:createUser,
    updateUser:updateUser,
    populateUser:populateUser,
    updateMulti:updateMulti,
};

