var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var cors = require('cors');
config = require('config')
var moment = require('moment');
var mongoose = require('mongoose');
var users = require('./routes/users.js');
md5 = require('md5');
var app = express();


process.env.NODE_ENV = 'development';

//dbconnection = require('./routes/connection.js');
var URL = 'mongodb://localhost:27017/assignment'
mongoose.connect(URL, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});



app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({ limit: "50mb",extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(express.static(__dirname + '/public'));

app.use(cors());

app.post('/sign_up',users.register);
app.post('/get_users',users.getUsers);
app.use('/image', multipartMiddleware)
app.post('/image', users.add);

app.get('/', function (req, res) {
    res.render('sign_up');
});



var server = http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});



