'use strict';
const async = require('async');
const sendResponse = require('./sendResponse.js');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const commonfunction = require('./commonfunction.js');
const validator = require('validator');
var fs = require('fs');
const url =require('url');
const Service = require('../Services');

exports.register = function (request, reply) {
    let email,name,password,address;
    async.auto({
        checkFields: function (cb) {
            if (!(request.body && request.body.email)) {
                cb("Please enter email")
            }
            else if (!(request.body && request.body.name)) {
                cb("Please enter name")
            }
            else if (!(request.body && request.body.password)) {
                cb("Please enter password");
            }
            else if (!(request.body && request.body.address)) {
                cb("Please enter password");
            }
            else if (!(request.body && request.body.cpassword)) {
                cb("Please enter confirm password");
            }
            else if(request.body.password!== request.body.cpassword) {
                cb('Confirm password is not same')
            }
            else {
                email = request.body.email;
                name = request.body.name;
                password = md5(request.body.password);
                address=request.body.address;
                cb(null);
            }
        },
        checkUser: ['checkFields', function (cb) {
            let criteria={
                email:email,
            };
            Service.UserServices.getUser(criteria,{},{lean:true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result.length){
                        cb('You are  already registered ')
                    }
                    else
                    cb()
                }
            })
        }],
        createUser: ['checkUser', function (cb) {
            let saveData={
               name:name,
                email:email,
                password:password,
                address:address
            };
            Service.UserServices.createUser(saveData, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                  cb()
                }
            })
        }],
    }, function (err, result) {
        if (err) {
            let msg = err;
            sendResponse.sendErrorMessage(msg, reply, 400);
        }
        else {
            sendResponse.sendSuccessMessage('Thankyou for registration', reply, 200);
        }
    })
};

exports.getUsers = function (request, reply) {
    let data={};
    async.auto({
        getUsers: function (cb) {
            let criteria={
            };
            Service.UserServices.getUser(criteria,{password:0,__v:0},{lean:true}, function (err, result) {
                if (err) {
                    cb(err);
                }
                else {
                    if(result.length){
                        data.data=result
                        data.userCount=result.length
                        cb()
                    }
                    else
                        cb('No data found')
                }
            })
        },
    }, function (err, result) {
        if (err) {
            let msg = err;
            sendResponse.sendErrorMessage(msg, reply, 400);
        }
        else {
            sendResponse.sendSuccessData(data,'success',reply, 200);
        }
    })
};

exports.add=function (request,reply) {
    var imagePath = request.files.img.path;

    var uploadPath='./upload/'+request.files.img.name;
    var readStream = fs.createReadStream(imagePath);
    var writeStream = fs.createWriteStream(uploadPath);
    readStream.on('data',function (dat) {
        writeStream.write(dat);
    })
    sendResponse.sendSuccessMessage("file is uploaded in "+uploadPath,reply,200);

}





