
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SchemaTypes = mongoose.Schema.Types;

var Users = new Schema({

    name: {type: String,required:true,trim: true},
    email: {type: String, trim: true,default:'',unique:true},
    password:{type: String,default:'',trim:true},
    address:{type:String,default:'',trim:true},
    registerDate:{type:Date,default: Date.now}
});


module.exports = mongoose.model('Users', Users);




